import 'package:flutter/material.dart';

void main() => runApp(Ahorcado());

class Ahorcado extends StatefulWidget {
  @override
  _AhorcadoState createState() => _AhorcadoState();
}

class _AhorcadoState extends State<Ahorcado> {
  final String palabraOculta = 'josh';

  String ayuda;

  //banderas para dibujar el ahorcado
  Map<String, bool> dibujar = {
    'cabeza': false,
    'tronco': false,
    'brazos': false,
    'piernas': false,
    'soga': false,
  };

  int maxIntentosFallidos;
  bool perdiste, adivinaste;

  @override
  void initState() {
    ayuda = '_' * palabraOculta.length;

    maxIntentosFallidos = dibujar.length;

    perdiste = false;
    adivinaste = false;
    print('numero de intentos que tienes para adivinar: $maxIntentosFallidos');

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Column(
            children: [
              Text('Palabra oculta: $ayuda'),
              Text('Numero de letras: ${palabraOculta.length}'),
            ],
          ),
        ),
        body: Center(
          child: Column(
            children: [
              (!adivinaste)
                  ? _showInput()
                  : Text(
                      'BUEN JUEGO!',
                      style: TextStyle(fontSize: 25),
                    ),
              SizedBox(height: 50),
              _dibujarAhorcado(),
              SizedBox(height: 25),
              (adivinaste)
                  ? _dibujarAdivinaste()
                  : (maxIntentosFallidos != 0)
                      ? Text(
                          'Solo puedes equivocarte $maxIntentosFallidos veces')
                      : SizedBox(height: 20)
            ],
          ),
        ),
      ),
    );
  }

  Widget _showInput() {
    return Container(
      child: Center(
        child: (!perdiste)
            ? Container(
                width: 40,
                child: TextField(
                  maxLengthEnforced: true,
                  onChanged: (letra) {
                    print(letra);
                    print('${letra.isEmpty ? 'vacio' : 'no vacio'}');
                    print(palabraOculta.characters);
                    print(ayuda[0]);
                    if (letra.isNotEmpty) {
                      if (palabraOculta.contains(letra)) {
                        print('adivinaste una letra!');
                        int indiceLetra = palabraOculta.indexOf(letra);
                        var cadena = ayuda.characters.toList();
                        print('indice de la letra: $indiceLetra');
                        setState(() {
                          print(
                              'length de la palabra: ${palabraOculta.length}');

                          print('length de la ayuda: ${ayuda.length}');
                          print(
                              'letra en la posicion de la ayuda: ${ayuda[indiceLetra]}');

                          cadena[indiceLetra] = letra;
                          print(cadena);
                          ayuda = cadena.join('');

                          print('ayuda -> $ayuda');
                          if (ayuda == palabraOculta) adivinaste = true;
                        });
                      } else {
                        print('te equivocaste!');

                        print('numero de intentos: $maxIntentosFallidos');
                        setState(() {
                          if (maxIntentosFallidos != 1) {
                            switch (maxIntentosFallidos) {
                              case 2:
                                {
                                  maxIntentosFallidos--;
                                  dibujar['piernas'] = true;
                                }
                                break;
                              case 3:
                                {
                                  maxIntentosFallidos--;
                                  dibujar['brazos'] = true;
                                }
                                break;
                              case 4:
                                {
                                  maxIntentosFallidos--;
                                  dibujar['tronco'] = true;
                                }
                                break;
                              case 5:
                                {
                                  maxIntentosFallidos--;
                                  dibujar['cabeza'] = true;
                                }
                                break;
                            }
                          } else {
                            maxIntentosFallidos--;
                            dibujar['soga'] = true;
                            perdiste = true;
                            print('perdiste!');
                          }
                        });
                      }
                    }
                  },
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    counterText: 'letra',
                    filled: true,
                  ),
                  maxLength: 1,
                ),
              )
            : Text(
                'Perdiste!',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.redAccent,
                ),
              ),
      ),
    );
  }

  Container _dibujarAhorcado() {
    return Container(
      child: Column(
        children: [
          (!dibujar['cabeza']) ? SizedBox(height: 25) : Text('cabeza'),
          (!dibujar['tronco']) ? SizedBox(height: 25) : Text('tronco'),
          (!dibujar['brazos']) ? SizedBox(height: 25) : Text('brazos'),
          (!dibujar['piernas']) ? SizedBox(height: 25) : Text('piernas'),
          (!dibujar['soga']) ? SizedBox(height: 25) : Text('soga'),
        ],
      ),
    );
  }

  Widget _dibujarAdivinaste() {
    return Text(
      'GANASTE!!!!!!',
      style: TextStyle(
        fontSize: 50,
        color: Colors.blueAccent,
      ),
    );
  }
}
